﻿namespace Masd.EAutoService.CustomersDataUSvc.Rest.Model
{
    public interface ICustomersDataService
    {
        public CustomerDTO GetCustomer(int searchText);
        public CustomerDTO[] GetCustomers();
        public void AddCustomer(string customerName, string customerSurname);

        public int GetCustomerId();

    }
}
