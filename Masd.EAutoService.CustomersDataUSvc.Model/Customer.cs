﻿namespace Masd.EAutoService.CustomersDataUSvc.Model
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; private set; }
        public string Surname { get; private set; }

        public Customer(int Id, string Name, string Surname)
        {
            this.Id = Id;
            this.Name = Name;
            this.Surname = Surname;
        }

    }
}