using Microsoft.AspNetCore.Mvc;
using Masd.EAutoService.CustomersDataUSvc.Model;
using Masd.EAutoService.CustomersDataUSvc.Logic;

namespace Masd.EAutoService.CustomersDataUSvc.Rest.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CustomerDataController : ControllerBase, ICustomersData
    {
        private readonly ILogger<CustomerDataController> logger;
        private readonly ICustomersData customersData;


        public CustomerDataController(ILogger<CustomerDataController> logger)
        {
            this.logger = logger;
            this.customersData = new CustomersData();
        }

        [HttpGet]
        [Route("GetCustomer")]
        public Customer GetCustomer(int id)
        {
            
            return customersData.GetCustomer(id);

        }


        [HttpGet]
        [Route("GetCustomers")]
        public Customer[] GetCustomers()
        {
            return customersData.GetCustomers();
        }

        [HttpGet]
        [Route("GetCustomerId")]
        public int GetCustomerId()
        {           
            return customersData.GetCustomers().Last().Id;
        }

        [HttpPost]
        [Route("AddCustomer")]
        public void AddCustomer(string customerName, string customerSurname)
        {
            customersData.AddCustomer(customerName,customerSurname);

        }


    }
}
