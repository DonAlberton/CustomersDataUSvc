﻿using Masd.EAutoService.CustomersDataUSvc.Model;

namespace Masd.EAutoService.CustomersDataUSvc.Logic
{
    public class CustomersData : ICustomersData
    {
        private static Customer[] Customers;
        private static readonly object customerDataLock = new object();
        private const string customersFilePath = "customers.json";

        static CustomersData()
        {
            lock (customerDataLock)
            {
                Customer[] customers = CustomersFileHandler.ReadJson(customersFilePath);
                Customers = customers;
            }
        }


        public Customer GetCustomer(int customerId)
        {
            lock (customerDataLock)
            {
                return Customers.FirstOrDefault(m => m.Id == customerId);
            }
        }

        public Customer[] GetCustomers()
        {
            lock (customerDataLock)
            {
                return Customers;
            }
        }
        public int GetCustomerId()
        {
            lock (customerDataLock)
            {
                return Customers.Last().Id;
            }
        }

        public void AddCustomer(string customerName, string customerSurname)
        {
            lock (customerDataLock)
            {
                Customer customer = new Customer(Customers.Last().Id + 1, customerName, customerSurname);
                List<Customer> customersList = new List<Customer>(Customers);

                customersList.Add(customer);

                Customer[] Or = customersList.ToArray();

                CustomersFileHandler.WriteCustomers(Or, "customers.json");

                Customers = Or;
            }
        }

    }
}