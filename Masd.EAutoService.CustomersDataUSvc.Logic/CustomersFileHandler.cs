﻿using Masd.EAutoService.CustomersDataUSvc.Model;
using System.Text.Json;

namespace Masd.EAutoService.CustomersDataUSvc.Logic
{
    public class CustomersFileHandler
    {
        public static Customer[] ReadJson(string fileName)
        {
            Customer[]? customer = JsonSerializer.Deserialize<Customer[]>(File.ReadAllText(fileName))!;
            return customer;
        }


        public static void WriteCustomers(Customer[] orders, string fileName)
        {

            string updatedJson = JsonSerializer.Serialize(orders, new JsonSerializerOptions { WriteIndented = true });

            File.WriteAllText(fileName, updatedJson);
        }

    }
}
