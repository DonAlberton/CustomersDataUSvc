﻿namespace Masd.EAutoService.CustomersDataUSvc.Model
{
    public interface ICustomersData
    {
        public void AddCustomer(string customerName, string customerSurname);
        public Customer GetCustomer(int id);

        public Customer[] GetCustomers();

        public int GetCustomerId();
    }
}
