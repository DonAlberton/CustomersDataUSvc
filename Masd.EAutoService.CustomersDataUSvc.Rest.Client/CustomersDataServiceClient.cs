﻿using System.Text.Json;
using System.Net.Http.Headers;
using Masd.EAutoService.CustomersDataUSvc.Rest.Model;

namespace Masd.EAutoService.CustomersDataUSvc.Rest.Client
{
    public class CustomersDataServiceClient : ICustomersDataService
    {
        private static readonly HttpClient httpClient = new HttpClient();
        private static readonly int port = 9001;
        public CustomerDTO GetCustomer(int searchText)
        {
            string webServiceUrl = String.Format("https://{0}:{1}/CustomerData/GetCustomer?id={2}", "localhost", port, searchText);

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            CustomerDTO customersData = ConvertJson1(jsonResponseContent);

            return customersData;
        }

        public CustomerDTO[] GetCustomers()
        {
            string webServiceUrl = String.Format("https://{0}:{1}/CustomerData/GetCustomers", "localhost", port);

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            CustomerDTO[] customersData = ConvertJson(jsonResponseContent);

            return customersData;
        }

        public int GetCustomerId()
        {
            string webServiceUrl = String.Format("https://{0}:{1}/CustomerData/GetCustomers", "localhost", port);

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            int customersData = int.Parse(jsonResponseContent);

            return customersData;
        }



        private async Task<string> CallWebService(HttpMethod httpMethod, string webServiceUrl)
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(httpMethod, webServiceUrl);

            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

            HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

            httpResponseMessage.EnsureSuccessStatusCode();

            string httpResponseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return httpResponseContent;
        }
        private async Task<string> CallWebService2(HttpMethod httpMethod, string webServiceUrl)
        {


            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, webServiceUrl);

            HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(request);


            httpResponseMessage.EnsureSuccessStatusCode();

            string httpResponseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return httpResponseContent;
        }
        public void AddCustomer(string customerName, string customerSurname)
        {

            string webServiceUrl =
                String.Format("https://localhost:{0}/CustomerData/AddCustomer?custmerName={1}&customerSurname={2}", port, customerName,customerSurname);

            Task<string> webServiceCall = CallWebService2(HttpMethod.Post, webServiceUrl);

            webServiceCall.Wait();

        }



        private CustomerDTO[] ConvertJson(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };

            CustomerDTO[] customersData = JsonSerializer.Deserialize<CustomerDTO[]>(json, options)!;

            return customersData;
        }
        private CustomerDTO ConvertJson1(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };

            CustomerDTO customersData = JsonSerializer.Deserialize<CustomerDTO>(json, options)!;

            return customersData;
        }
    }
}